angular.module("root",[])
.controller("RootCtrl", ["$scope", "$window", "$rootScope", 
  function ($scope, $window, $rootScope){
    
    $(".content").css("height", $rootScope.contentHeight);

    $scope.enforceStrongPassword = false;
    $scope.$watch("enforceStrongPassword", function(value){
      $scope.isSamePassword = false;
      $rootScope.installationData.rootPassword = "";
      $rootScope.installationData.rootPasswordRepeat = "";
      $scope.passwordStrength = false;
    })
    $scope.$watch("installationData.rootPassword", function(value){
      $scope.isSamePassword = false;
      $rootScope.installationData.rootPasswordRepeat = "";
      $rootScope.personalizationError = false;
      if (value && $scope.enforceStrongPassword) {
        console.log(value);
        if (value.length >= 8) {
          $scope.validPassword = true;
        } else {
          $scope.passwordStrength = "weak";
          if ($scope.enforceStrongPassword) {
            $scope.validPassword = false;
          }
        }
        if (value.length >= 8) {
          $scope.passwordStrength = "strong"; 
          if ($scope.enforceStrongPassword) {
            $scope.validPassword = false;
          }
        }
        if (value.match(/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/g) && value.length >= 8) {
          $scope.passwordStrength = "veryStrong"; 
          $scope.validPassword = true;
        }
        console.log($scope.passwordStrength);
      }
    });
    $scope.$watch("installationData.rootPasswordRepeat", function(value){
      $rootScope.personalizationError = false;
      if (value && value == $scope.installationData.rootPassword) {
        $scope.isSamePassword = true;
      } else {
        $scope.isSamePassword = false;
      }
    });
    $scope.validatePersonalization = function(installationData, validPassword, isSamePassword) {
      $rootScope.personalizationError = false;
      if (isSamePassword) {
        if ($scope.enforceStrongPassword) {
          if (validPassword) {
            $rootScope.next();
          } else {
            $rootScope.personalizationError = true;
          }
        } else {
          $rootScope.next();
        }
      } else {
        $rootScope.personalizationError = true;
      }
    }
    
}])
