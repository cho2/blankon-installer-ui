angular.module("partition",[])
.controller("PartitionCtrl", ["$scope", "$window", "$timeout", "$rootScope", 
  function ($scope, $window, $timeout, $rootScope){
    
    $(".content").css("height", $rootScope.contentHeight);
    
    $rootScope.installationData.secureInstall = true;
    $rootScope.installationData.secureInstallPassphrase = "";
    $rootScope.installationData.secureInstallPassphraseRepeat = "";

    $scope.actionDialog = false;
    $scope.title = "Installation Target";
    var gbSize = 1073741824;
    var minimumPartitionSize = 4 * gbSize;
    var driveBlockWidth = 600;
    
    $scope.partitionSimpleNext = function(){
      if ($rootScope.selectedInstallationTarget) {
        $rootScope.next(); 
      }
    }
  
    if (!$rootScope.installationData.partition) {
      // give time for transition
      $timeout(function(){
        if (window.Parted) {
          $rootScope.devices = Parted.getDevices();
        } else {
          // Used for debugging
          $rootScope.devices = [{"path":"/dev/sda","size":53687091200,"model":"ATA VBOX HARDDISK","label":"msdos","partitions":[{"id":-1,"parent":-1,"start":32256,"end":1048064,"size":1016320,"type":"DEVICE_PARTITION_TYPE_FREESPACE","filesystem":"","description":""},{"id":1,"parent":-1,"start":1048576,"end":15570304512,"size":15569256448,"type":"DEVICE_PARTITION_TYPE_NORMAL","filesystem":"ext4","description":""},{"id":2,"parent":-1,"start":15570305024,"end":17780702720,"size":2210398208,"type":"DEVICE_PARTITION_TYPE_NORMAL","filesystem":"ext4","description":""},{"id":-1,"parent":-1,"start":17780703232,"end":27044871680,"size":9264168960,"type":"DEVICE_PARTITION_TYPE_FREESPACE","filesystem":"","description":""},{"id":3,"parent":-1,"start":27044872192,"end":53687090688,"size":26642219008,"type":"DEVICE_PARTITION_TYPE_EXTENDED","filesystem":"","description":""},{"id":-1,"parent":-1,"start":27044872192,"end":27044872192,"size":512,"type":"DEVICE_PARTITION_TYPE_FREESPACE","filesystem":"","description":""},{"id":-1,"parent":-1,"start":27044872704,"end":27045920256,"size":1048064,"type":"DEVICE_PARTITION_TYPE_FREESPACE","filesystem":"","description":""},{"id":5,"parent":-1,"start":27045920768,"end":50703891968,"size":23657971712,"type":"DEVICE_PARTITION_TYPE_LOGICAL","filesystem":"ext4","description":""},{"id":-1,"parent":-1,"start":50703892480,"end":53687090688,"size":2983198720,"type":"DEVICE_PARTITION_TYPE_FREESPACE","filesystem":"","description":""}],"$$hashKey":"00T"}];
        }
        $scope.scanning = true;
      }, 1000);
    }
    $scope.setDrive = function(drive) {
      $rootScope.validInstallationTarget = true;
      // TODO : reset UI
      $rootScope.installationData.device = $rootScope.devices.indexOf(drive);
      var path = drive.path;
      $rootScope.installationData.device_path = path;
      console.log(JSON.stringify($rootScope.devices));
      for (i = 0; i < $rootScope.devices.length; i++)
        if ($rootScope.devices[i].path === path) {
          $rootScope.selectedDrive = $rootScope.devices[i];
          $rootScope.selectedDrive.id = i;
          $rootScope.selectedDrive.partitionList = [];
          $rootScope.selectedDrive.driveWidth = 8 + 1; // Add 1 pixel tolerance
          $rootScope.selectedDrive.sizeGb = $rootScope.selectedDrive.size * gbSize;
          $rootScope.selectedDrive.hasExtended = false;
          for (j = 0; j < $rootScope.selectedDrive.partitions.length; j++) {
            var p = $rootScope.selectedDrive.partitions[j];
            $rootScope.selectedDrive.partitionList.push(p);
            // filter partition to fit requirements
            if ( 
              (p.type.indexOf("NORMAL") > 0 || p.type.indexOf("LOGICAL") > 0 || p.type.indexOf("FREESPACE") > 0) && p.size > (0.01*gbSize)) {
              p.blockWidth = parseInt(((p.size/$rootScope.selectedDrive.size)*driveBlockWidth));
              $rootScope.selectedDrive.driveWidth += (p.blockWidth);
              p.sizeGb = (p.size/gbSize).toFixed(2);
              p.selected = false;
              p.normal = true;
              if (p.type.indexOf("LOGICAL") > 0) {
                p.logical = true;
                if ($rootScope.selectedDrive.hasExtended) {
                  for (var k = 0; k < $rootScope.selectedDrive.partitionList.length; k++) {
                    if ($rootScope.selectedDrive.partitionList[k].extended &&
                    p.start >= $rootScope.selectedDrive.partitionList[k].start &&
                    p.end <= $rootScope.selectedDrive.partitionList[k].end) {
                      // tell it that it has child(s);
                      $rootScope.selectedDrive.partitionList[k].hasChild = true;
                    }
                  }
                }
              } 
              if (p.size < minimumPartitionSize) {
                p.disallow = true;
              }
              if (p.id < 1 && p.type.indexOf("FREESPACE") > 0) {
                p.freespace = true;
                // is this freespace a child of extended partition?
                if ($rootScope.selectedDrive.hasExtended) {
                  for (var k = 0; k < $rootScope.selectedDrive.partitionList.length; k++) {
                    if ($rootScope.selectedDrive.partitionList[k].extended &&
                    p.start >= $rootScope.selectedDrive.partitionList[k].start &&
                    p.end <= $rootScope.selectedDrive.partitionList[k].end) {
                      p.logicalFreespace = true;
                    }
                  }
                }
              }
            } else {
              if (p.type.indexOf("EXTENDED") > 0) {
                p.extended = true;
                $rootScope.selectedDrive.hasExtended = true;
              } else {
                p.hidden = true;
              } 
            }
          }
        }
      } 
      $scope.validate = function() {
        if (!$rootScope.validInstallationTarget) {
          return;
        }
        if (!$rootScope.installationData.secureInstallPassphrase) {
          return;
        } else {
          if ($rootScope.installationData.secureInstallPassphrase != $rootScope.installationData.secureInstallPassphraseRepeat) {
            return;
          }
        }

        $rootScope.next();
      }   
      if ($rootScope.quickDebug) {
        $rootScope.installationData.secureInstallPassphrase = "test";
        $rootScope.installationData.secureInstallPassphraseRepeat = "test";
        setTimeout(function(){
          $scope.setDrive($rootScope.devices[0]);
          $rootScope.next();
        }, 1000);
      }
    }

])
